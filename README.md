# Augmented Patient
This repository contains the visual studio solution that will build a copy of our Augmented Patient nursing student trainer. It also contains a copy of the appbundle required for sideloading the application if you do not want to edit it yourself.

For detailed instructions on building, installing, and use, please check out the [GitLab Wiki](https://gitlab.com/pd3d/augmented-patient/-/wikis/home) that contains all of the documentation for this project.